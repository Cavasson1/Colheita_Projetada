package tk.colheitaProjetada.colheitaProjetada;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColheitaProjetadaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColheitaProjetadaApplication.class, args);
	}
}
