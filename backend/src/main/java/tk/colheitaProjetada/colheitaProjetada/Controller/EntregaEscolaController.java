package tk.colheitaProjetada.colheitaProjetada.Controller;

import java.sql.SQLException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tk.colheitaProjetada.colheitaProjetada.DAO.EntregaEscolaDAO;
import tk.colheitaProjetada.colheitaProjetada.Model.EntregaEscola;
import tk.colheitaProjetada.colheitaProjetada.Model.Retorno;

@RestController
@CrossOrigin
@RequestMapping("/app/entregaEscola")
public class EntregaEscolaController implements IController<EntregaEscola> {
	EntregaEscolaDAO entregaEscolaDAO = new EntregaEscolaDAO();

	private Retorno retorno;

	@Override
	@PostMapping(value = { "/salvar" }, produces = "application/json")
	public ResponseEntity<Retorno> inserir(@RequestBody EntregaEscola objeto) {
		retorno = new Retorno();
		try {
			retorno.setData(entregaEscolaDAO.inserir(objeto));

			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);
		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@Override
	@PutMapping(value = { "/salvar" }, produces = "application/json")
	public ResponseEntity<Retorno> salvar(@RequestBody EntregaEscola objeto) {
		retorno = new Retorno();
		try {
			retorno.setData(entregaEscolaDAO.salvar(objeto));

			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);
		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@Override
	@GetMapping(value = { "/listar/{codigo}" }, produces = "application/json")
	public ResponseEntity<Retorno> buscar(@PathVariable int codigo) {
		retorno = new Retorno();
		try {
			retorno.setData(entregaEscolaDAO.buscar(codigo));
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);

		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@Override
	public ResponseEntity<Retorno> excluir(int codigo) {
		retorno = new Retorno();
		try {
			entregaEscolaDAO.excluir(codigo);
			retorno.setMensagem("Excluído com Sucesso");
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);
		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@GetMapping(value = { "/listarTodos/{codigo}" }, produces = "application/json")
	public ResponseEntity<Retorno> listar(@PathVariable int codigo) {
		retorno = new Retorno();
		try {
			retorno.setData(entregaEscolaDAO.listar(codigo));
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);

		} catch (SQLException error) {
			return formatError(error);
		}
	}
	
	@Override
	public ResponseEntity<Retorno> listar() {
		return null;
	}

	private ResponseEntity<Retorno> formatError(SQLException error) {
		retorno.setMensagem(error.getMessage());
		return new ResponseEntity<Retorno>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
