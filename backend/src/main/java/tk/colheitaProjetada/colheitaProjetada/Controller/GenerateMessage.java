package tk.colheitaProjetada.colheitaProjetada.Controller;

import java.sql.SQLException;

import javax.swing.JOptionPane;

public abstract class GenerateMessage {
	public boolean success() {
		JOptionPane.showMessageDialog(null, "Procedimento Executado com Sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
		return true;
	}
	
	public boolean error(SQLException error) {
		JOptionPane.showMessageDialog(null, error.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
		return false;
	}
	
	public boolean error(Exception error) {
		JOptionPane.showMessageDialog(null, error.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
		return false;
	}
}
