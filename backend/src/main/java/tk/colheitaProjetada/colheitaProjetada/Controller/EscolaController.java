package tk.colheitaProjetada.colheitaProjetada.Controller;

import java.sql.SQLException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tk.colheitaProjetada.colheitaProjetada.DAO.EscolaDAO;
import tk.colheitaProjetada.colheitaProjetada.Model.Escola;
import tk.colheitaProjetada.colheitaProjetada.Model.Retorno;

@RestController
@CrossOrigin
@RequestMapping ( "/app/escola" )
public class EscolaController implements IController<Escola>{
	EscolaDAO escolaDAO = new EscolaDAO();
	private Retorno retorno;

	@Override
	@PostMapping(value = { "/salvar" }, produces = "application/json")
	public ResponseEntity<Retorno> inserir(@RequestBody Escola objeto) {
		retorno = new Retorno();
		try {
			retorno.setData(escolaDAO.inserir(objeto));

			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);
		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@Override
	@PutMapping(value = { "/salvar" }, produces = "application/json")
	public ResponseEntity<Retorno> salvar(@RequestBody Escola objeto) {
		retorno = new Retorno();
		try {
			retorno.setData(escolaDAO.salvar(objeto));

			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);
		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@Override
	@GetMapping(value = { "/listar/{codigo}" }, produces = "application/json")
	public ResponseEntity<Retorno> buscar(@PathVariable int codigo) {
		retorno = new Retorno();
		try {
			retorno.setData(escolaDAO.buscar(codigo));
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);

		} catch (SQLException error) {
			return formatError(error);
		}
	}
	
	@Override
	@DeleteMapping(value = { "/deletar/{codigo}" }, produces = "application/json")
	public ResponseEntity<Retorno> excluir(@PathVariable int codigo) {
		retorno = new Retorno();
		try {
			escolaDAO.excluir(codigo);
			retorno.setMensagem("Excluído com Sucesso");
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);
		} catch (SQLException error) {
			return formatError(error);
		}
	}

	@Override
	@GetMapping(value = { "/listar" }, produces = "application/json")
	public ResponseEntity<Retorno> listar() {
		retorno = new Retorno();
		try {
			retorno.setData(escolaDAO.listar());
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);

		} catch (SQLException error) {
			return formatError(error);
		}
	}

	private ResponseEntity<Retorno> formatError(SQLException error) {
		retorno.setMensagem(error.getMessage());
		return new ResponseEntity<Retorno>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
