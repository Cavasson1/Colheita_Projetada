package tk.colheitaProjetada.colheitaProjetada.Controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.http.ResponseEntity;

import tk.colheitaProjetada.colheitaProjetada.Model.Retorno;

public interface IController<Entity> {

	public abstract ResponseEntity<Retorno> buscar(int codigo) throws SQLException;

	public abstract ResponseEntity<Retorno> inserir(Entity objeto) throws SQLException;

	public abstract ResponseEntity<Retorno> salvar(Entity objeto) throws SQLException;

	public abstract ResponseEntity<Retorno> excluir(int codigo) throws SQLException;

	public abstract ResponseEntity<Retorno> listar() throws SQLException;
}
