package tk.colheitaProjetada.colheitaProjetada.Controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tk.colheitaProjetada.colheitaProjetada.Model.Login;
import tk.colheitaProjetada.colheitaProjetada.Model.Retorno;

@RestController
public class LoginController extends HttpEntity<Login> {

	@CrossOrigin
	@PostMapping(value = { "/app/login" }, produces = "application/json")
	public ResponseEntity<Retorno> login(@RequestBody Login objeto) {
		Retorno retorno = new Retorno();
		try {

			retorno.setData(Login.login(objeto));

			if (retorno.getData() != null)
				return new ResponseEntity<Retorno>(retorno, HttpStatus.ACCEPTED);

		} catch (Exception error) {
			retorno.setMensagem(error.getMessage());
			return new ResponseEntity<Retorno>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Retorno>(retorno, HttpStatus.BAD_REQUEST);
	}

}
