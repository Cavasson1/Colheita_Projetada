package tk.colheitaProjetada.colheitaProjetada.Controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tk.colheitaProjetada.colheitaProjetada.DAO.EntregaDAO;
import tk.colheitaProjetada.colheitaProjetada.Model.Entrega;
import tk.colheitaProjetada.colheitaProjetada.Model.Retorno;

@RestController
@CrossOrigin
@RequestMapping("/app/entrega")
public class EntregaController extends GenerateMessage {
	EntregaDAO entregaDAO = new EntregaDAO();
	Retorno retorno;
	
	@GetMapping(value = { "/listar" }, produces = "application/json")
	public List<Entrega> listar() {
		try {
			return entregaDAO.listar();
		} catch (SQLException error) {
			error(error);
			return null;
		}
	}
	
	@GetMapping(value = { "/dados" }, produces = "application/json")
	public ResponseEntity<Retorno> dados() {
		retorno = new Retorno();
		try {
			retorno.setData(entregaDAO.dados());
			return new ResponseEntity<Retorno>(retorno, HttpStatus.OK);

		} catch (SQLException error) {
			return formatError(error);
		}
	}
	
	private ResponseEntity<Retorno> formatError(SQLException error) {
		retorno.setMensagem(error.getMessage());
		return new ResponseEntity<Retorno>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
