package tk.colheitaProjetada.colheitaProjetada.Model;

import tk.colheitaProjetada.colheitaProjetada.DAO.LoginDAO;

public class Login {
	private int idUsuario;
	private String usuario;
	private String senha;
	private String permission;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getPermission() {
		return permission;
	}
	
	public void setPermission(String permission) {
		this.permission = permission;
	}

	public static Login login(Login objeto) throws Exception {
		LoginDAO loginDAO = new LoginDAO();
		
		return loginDAO.login(objeto);
	}
}
