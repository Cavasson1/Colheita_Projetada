package tk.colheitaProjetada.colheitaProjetada.Model;

public class Produto {
	private int idProduto;
	private String nome;
	private String orientPlantio;
	private double sementePorKg;
	private int diasIniProd;
	private int diasFinProd;
	private double valor;

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getOrientPlantio() {
		return orientPlantio;
	}

	public void setOrientPlantio(String orientPlantio) {
		this.orientPlantio = orientPlantio;
	}

	public double getSementePorKg() {
		return sementePorKg;
	}

	public void setSementePorKg(double sementePorKg) {
		this.sementePorKg = sementePorKg;
	}

	public int getDiasIniProd() {
		return diasIniProd;
	}

	public void setDiasIniProd(int diasIniProd) {
		this.diasIniProd = diasIniProd;
	}

	public int getDiasFinProd() {
		return diasFinProd;
	}

	public void setDiasFinProd(int diasFinProd) {
		this.diasFinProd = diasFinProd;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
