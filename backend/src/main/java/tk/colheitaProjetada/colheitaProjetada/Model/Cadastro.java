package tk.colheitaProjetada.colheitaProjetada.Model;

import tk.colheitaProjetada.colheitaProjetada.DAO.CadastroDAO;

public class Cadastro {
	private String nome;
	private String senha;
	private String permission;
	private String numDAP;

	public String getNome() {
		return nome;
	}

	public void setNome(String usuario) {
		this.nome = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getPermission() {
		return permission;
	}
	
	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	public String getNumDAP() {
		return numDAP;
	}
	
	public void setNumDAP(String numDAP) {
		this.numDAP = numDAP;
	}

	public static Login cadastrar(Cadastro objeto) throws Exception {
		CadastroDAO cadastroDAO = new CadastroDAO();
		
		return cadastroDAO.cadastro(objeto);
	}
}
