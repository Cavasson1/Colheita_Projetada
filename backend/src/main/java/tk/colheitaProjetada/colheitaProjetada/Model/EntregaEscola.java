package tk.colheitaProjetada.colheitaProjetada.Model;

import java.sql.Date;

public class EntregaEscola {
	private int idEntregaEscola;
	private Date dataEntrega;
	private int idEscola;
	private int idProdutor;
	private int idProduto;
	private double quantidade;
	private String produto;
	private String produtor;
	private String escola;

	public int getIdEntregaEscola() {
		return idEntregaEscola;
	}

	public void setIdEntregaEscola(int idEntregaEscola) {
		this.idEntregaEscola = idEntregaEscola;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public int getIdEscola() {
		return idEscola;
	}

	public void setIdEscola(int idEscola) {
		this.idEscola = idEscola;
	}

	public int getIdProdutor() {
		return idProdutor;
	}

	public void setIdProdutor(int idProdutor) {
		this.idProdutor = idProdutor;
	}

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getProdutor() {
		return produtor;
	}

	public void setProdutor(String produtor) {
		this.produtor = produtor;
	}
	
	public String getEscola() {
		return escola;
	}
	
	public void setEscola(String escola) {
		this.escola = escola;
	}

}
