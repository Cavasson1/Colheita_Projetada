package tk.colheitaProjetada.colheitaProjetada.Model;

public class Entrega {
	private int codigoEntrega;
	private int idEscola;
	private String nomeEscola;
	private int idProdutor;
	private String nomeProdutor;

	public int getCodigoEntrega() {
		return codigoEntrega;
	}

	public void setCodigoEntrega(int codigoEntrega) {
		this.codigoEntrega = codigoEntrega;
	}

	public int getIdEscola() {
		return idEscola;
	}

	public void setIdEscola(int idEscola) {
		this.idEscola = idEscola;
	}

	public String getNomeEscola() {
		return nomeEscola;
	}

	public void setNomeEscola(String nomeEscola) {
		this.nomeEscola = nomeEscola;
	}

	public int getIdProdutor() {
		return idProdutor;
	}

	public void setIdProdutor(int idProdutor) {
		this.idProdutor = idProdutor;
	}

	public String getNomeProdutor() {
		return nomeProdutor;
	}

	public void setNomeProdutor(String nomeProdutor) {
		this.nomeProdutor = nomeProdutor;
	}

}
