package tk.colheitaProjetada.colheitaProjetada.Model;

import java.util.ArrayList;

public class Retorno {
	private Object data;
	private String mensagem;

	public Retorno(Object data, String mensagem) {
		this.data = data;
		this.mensagem = mensagem;
	}

	public Retorno() {

	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void setData(ArrayList<Object> data) {
		this.data = data;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
