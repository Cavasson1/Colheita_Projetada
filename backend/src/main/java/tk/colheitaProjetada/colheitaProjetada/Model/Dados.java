package tk.colheitaProjetada.colheitaProjetada.Model;

import java.util.ArrayList;

public class Dados {
	private ArrayList<Produto> produto;
	private ArrayList<Produtor> produtor;
	private ArrayList<Escola> escola;
	
	public Dados() {
		this.produto = new ArrayList<Produto>();
		this.produtor = new ArrayList<Produtor>();
		this.escola = new ArrayList<Escola>();
	}

	public ArrayList<Produto> getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto.add(produto);
	}

	public ArrayList<Produtor> getProdutor() {
		return produtor;
	}

	public void setProdutor(Produtor produtor) {
		this.produtor.add(produtor);
	}

	public ArrayList<Escola> getEscola() {
		return escola;
	}

	public void setEscola(Escola escola) {
		this.escola.add(escola);
	}
}
