package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONException;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.Login;

public class LoginDAO {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;

	public Login login(Login objeto) throws SQLException, JSONException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "select nome, permission " //
					+ "from usuario " //
					+ "where nome = ? " //
					+ "and senha = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getUsuario());
			sqlParametro.setString(2, objeto.getSenha());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				Login login = new Login();
				login.setUsuario(resultado.getString("nome"));
				login.setPermission(resultado.getString("permission"));
				return login;
			} else {
				sql = "select idProdutor, nome\r\n" + 
						"from produtor\r\n" + 
						"where nome = ?\r\n" + 
						"and senha = ?;";

				sqlParametro = conexao.prepareStatement(sql);

				sqlParametro.setString(1, objeto.getUsuario());
				sqlParametro.setString(2, objeto.getSenha());

				resultado = sqlParametro.executeQuery();

				if (resultado.next()) {
					Login login = new Login();
					login.setIdUsuario(resultado.getInt("idProdutor"));
					login.setUsuario(resultado.getString("nome"));
					login.setPermission("user");
					return login;
				}
			}
			return null;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
}
