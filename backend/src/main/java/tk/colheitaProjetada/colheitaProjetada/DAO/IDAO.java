package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.SQLException;
import java.util.List;

public interface IDAO<Entity> {
	public abstract Entity inserir(Entity objeto) throws SQLException;
	
	public abstract Entity salvar(Entity objeto) throws SQLException;

	public abstract void excluir(int codigo) throws SQLException;
	
	public abstract List<Entity> listar() throws SQLException;
	
	public abstract Entity buscar(int codigo) throws SQLException;
	
}
