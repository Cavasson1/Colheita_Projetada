package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.Escola;

public class EscolaDAO implements IDAO<Escola>{
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;
	private List<Escola> lista;
	private Escola escola;

	@Override
	public Escola inserir(Escola objeto) throws SQLException {
		conexao = new Conexao().geraConexao();
		try {
			sql = "insert into escola (nome, email, telefone1, telefone2, rua, bairro, cidade, uf, cep, numero, complemento) \r\n" + 
					"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getEmail());
			sqlParametro.setString(3, objeto.getTelefone1());
			sqlParametro.setString(4, objeto.getTelefone2());
			sqlParametro.setString(5, objeto.getRua());
			sqlParametro.setString(6, objeto.getBairro());
			sqlParametro.setString(7, objeto.getCidade());
			sqlParametro.setString(8, objeto.getUf());
			sqlParametro.setString(9, objeto.getCep());
			sqlParametro.setString(10, objeto.getNumero());
			sqlParametro.setString(11, objeto.getComplemento());

			sqlParametro.executeUpdate();
			
			sql = "select * " //
					+ "from escola " //
					+ "where idEscola = (" //
					+ "select last_insert_id() " //
					+ "as idEscola" //
					+ ");";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				escola = new Escola();
				escola.setIdEscola(resultado.getInt("idEscola"));
				escola.setNome(resultado.getString("nome"));
				escola.setEmail(resultado.getString("email"));
				escola.setTelefone1(resultado.getString("telefone1"));
				escola.setTelefone2(resultado.getString("telefone2"));
				escola.setUf(resultado.getString("uf"));
				escola.setCidade(resultado.getString("cidade"));
				escola.setBairro(resultado.getString("bairro"));
				escola.setCep(resultado.getString("cep"));
				escola.setNumero(resultado.getString("numero"));
				escola.setRua(resultado.getString("rua"));
				escola.setComplemento(resultado.getString("complemento"));

				return escola;
			}
			
			return null;
			
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Escola salvar(Escola objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {
			
			sql = "update escola " //
					+ "set " //
					+ "nome = ?, " //
					+ "email = ?, " //
					+ "telefone1 = ?, " //
					+ "telefone2 = ?, " //
					+ "rua = ?, " //
					+ "bairro = ?, " //
					+ "cidade = ?, " //
					+ "uf = ?, " //
					+ "cep = ?, " //
					+ "numero = ?, " //
					+ "complemento = ? " //
					+ "where idEscola = ?";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getEmail());
			sqlParametro.setString(3, objeto.getTelefone1());
			sqlParametro.setString(4, objeto.getTelefone2());
			sqlParametro.setString(5, objeto.getRua());
			sqlParametro.setString(6, objeto.getBairro());
			sqlParametro.setString(7, objeto.getCidade());
			sqlParametro.setString(8, objeto.getUf());
			sqlParametro.setString(9, objeto.getCep());
			sqlParametro.setString(10, objeto.getNumero());
			sqlParametro.setString(11, objeto.getComplemento());
			sqlParametro.setInt(12, objeto.getIdEscola());

			sqlParametro.executeUpdate();
			
			sql = "select * " //
					+ "from escola " //
					+ "where idEscola = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, objeto.getIdEscola());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				escola = new Escola();
				escola.setIdEscola(resultado.getInt("idEscola"));
				escola.setNome(resultado.getString("nome"));
				escola.setEmail(resultado.getString("email"));
				escola.setTelefone1(resultado.getString("telefone1"));
				escola.setTelefone2(resultado.getString("telefone2"));
				escola.setUf(resultado.getString("uf"));
				escola.setCidade(resultado.getString("cidade"));
				escola.setBairro(resultado.getString("bairro"));
				escola.setCep(resultado.getString("cep"));
				escola.setNumero(resultado.getString("numero"));
				escola.setRua(resultado.getString("rua"));
				escola.setComplemento(resultado.getString("complemento"));

				return escola;
			}

			return null;
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "delete from escola " //
					+ "where idEscola = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);
			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Escola> listar() throws SQLException {
		conexao = new Conexao().geraConexao();
		lista = new ArrayList<Escola>();

		try {

			sql = "select * " //
					+ "from escola;";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				escola = new Escola();
				escola.setIdEscola(resultado.getInt("idEscola"));
				escola.setNome(resultado.getString("nome"));
				escola.setEmail(resultado.getString("email"));
				escola.setTelefone1(resultado.getString("telefone1"));
				escola.setTelefone2(resultado.getString("telefone2"));
				escola.setUf(resultado.getString("uf"));
				escola.setCidade(resultado.getString("cidade"));
				escola.setBairro(resultado.getString("bairro"));
				escola.setCep(resultado.getString("cep"));
				escola.setNumero(resultado.getString("numero"));
				escola.setRua(resultado.getString("rua"));
				escola.setComplemento(resultado.getString("complemento"));

				lista.add(escola);
			}
			return lista;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Escola buscar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {
			sql = "select * " //
					+ "from escola " //
					+ "where idEscola = ?;";
			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				escola = new Escola();
				escola.setIdEscola(resultado.getInt("idEscola"));
				escola.setNome(resultado.getString("nome"));
				escola.setEmail(resultado.getString("email"));
				escola.setTelefone1(resultado.getString("telefone1"));
				escola.setTelefone2(resultado.getString("telefone2"));
				escola.setUf(resultado.getString("uf"));
				escola.setCidade(resultado.getString("cidade"));
				escola.setBairro(resultado.getString("bairro"));
				escola.setCep(resultado.getString("cep"));
				escola.setNumero(resultado.getString("numero"));
				escola.setRua(resultado.getString("rua"));
				escola.setComplemento(resultado.getString("complemento"));

				return escola;
			}

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
