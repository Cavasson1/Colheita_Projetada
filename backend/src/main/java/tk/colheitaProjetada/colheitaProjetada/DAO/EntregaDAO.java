package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.Dados;
import tk.colheitaProjetada.colheitaProjetada.Model.Entrega;
import tk.colheitaProjetada.colheitaProjetada.Model.Escola;
import tk.colheitaProjetada.colheitaProjetada.Model.Produto;
import tk.colheitaProjetada.colheitaProjetada.Model.Produtor;

public class EntregaDAO {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;
	private List<Entrega> lista;
	private Entrega entrega;

	public List<Entrega> listar() throws SQLException {
		conexao = new Conexao().geraConexao();
		lista = new ArrayList<Entrega>();

		try {

			sql = "select escola_idEscola, escola.nome, produtor_idProdutor, produtor.nome\r\n" + 
					"from entrega_produtor\r\n" + 
					"join escola\r\n" + 
					"on escola_idEscola = idEscola\r\n" + 
					"join produtor\r\n" + 
					"on produtor_idProdutor = idProdutor\r\n" +
					"group by produtor.nome, escola.nome";//

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			int cod = 1;
			while (resultado.next()) {
				entrega = new Entrega();
				entrega.setCodigoEntrega(cod);
				entrega.setIdEscola(resultado.getInt("escola_idEscola"));
				entrega.setNomeEscola(resultado.getString("escola.nome"));
				entrega.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entrega.setNomeProdutor(resultado.getString("produtor.nome"));
				
				lista.add(entrega);
				cod++;
			}
			return lista;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public Dados dados() throws SQLException {
		conexao = new Conexao().geraConexao();
		
		Dados dados = new Dados();

		try {

			sql = "select * " //
					+ "from produto;";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				Produto produto = new Produto();
				produto.setIdProduto(resultado.getInt("idProduto"));
				produto.setNome(resultado.getString("nome"));
				produto.setOrientPlantio(resultado.getString("orientPlantio"));
				produto.setSementePorKg(resultado.getDouble("sementePorKg"));
				produto.setDiasIniProd(resultado.getInt("diasIniProd"));
				produto.setDiasFinProd(resultado.getInt("diasFinProd"));
				produto.setValor(resultado.getDouble("valor"));

				dados.setProduto(produto);
			}
			
			sql = "select * " //
					+ "from produtor;";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				Produtor produtor = new Produtor();
				produtor.setIdProdutor(resultado.getInt("idProdutor"));
				produtor.setNome(resultado.getString("nome"));
				produtor.setNumDAP(resultado.getString("numDAP"));
				produtor.setCelular(resultado.getString("celular"));
				produtor.setTelefone(resultado.getString("Telefone"));
				produtor.setEmail(resultado.getString("email"));
				produtor.setSexo(resultado.getString("sexo"));
				produtor.setSenha(resultado.getString("senha"));
				produtor.setUf(resultado.getString("uf"));
				produtor.setCidade(resultado.getString("cidade"));
				produtor.setBairro(resultado.getString("bairro"));
				produtor.setCep(resultado.getString("cep"));
				produtor.setNumero(resultado.getString("numero"));
				produtor.setRua(resultado.getString("rua"));
				produtor.setReferencia(resultado.getString("referencia"));

				dados.setProdutor(produtor);
			}
			
			sql = "select * " //
					+ "from escola;";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				Escola escola = new Escola();
				escola.setIdEscola(resultado.getInt("idEscola"));
				escola.setNome(resultado.getString("nome"));
				escola.setEmail(resultado.getString("email"));
				escola.setTelefone1(resultado.getString("telefone1"));
				escola.setTelefone2(resultado.getString("telefone2"));
				escola.setUf(resultado.getString("uf"));
				escola.setCidade(resultado.getString("cidade"));
				escola.setBairro(resultado.getString("bairro"));
				escola.setCep(resultado.getString("cep"));
				escola.setNumero(resultado.getString("numero"));
				escola.setRua(resultado.getString("rua"));
				escola.setComplemento(resultado.getString("complemento"));

				dados.setEscola(escola);
			}
			
			return dados;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
}
