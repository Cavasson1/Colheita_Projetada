package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.EntregaEscola;

public class EntregaEscolaDAO {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;
	private List<EntregaEscola> lista;
	private EntregaEscola entregaEscola;

	public EntregaEscola inserir(EntregaEscola objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "insert into entrega_escola (dataEntrega, escola_idEscola, produtor_idProdutor, produto_idProduto, quantidade) "
					+ "values (?, ?, ?, ?, ?);";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setDate(1, objeto.getDataEntrega());
			sqlParametro.setInt(2, objeto.getIdEscola());
			sqlParametro.setInt(3, objeto.getIdProdutor());
			sqlParametro.setInt(4, objeto.getIdProduto());
			sqlParametro.setDouble(5, objeto.getQuantidade());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from entrega_escola " //
					+ "where idEntregaEscola = (" //
					+ "select last_insert_id() " //
					+ "as idEntregaEscola" //
					+ ");";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				entregaEscola = new EntregaEscola();
				entregaEscola.setIdEntregaEscola(resultado.getInt("idEntregaEscola"));
				entregaEscola.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaEscola.setIdEscola(resultado.getInt("escola_idEscola"));
				entregaEscola.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaEscola.setIdProduto(resultado.getInt("produto_idProduto"));
				entregaEscola.setQuantidade(resultado.getDouble("quantidade"));
			}

			return entregaEscola;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public EntregaEscola salvar(EntregaEscola objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "update entrega_escola " //
					+ "set " + "dataEntrega = ?, " //
					+ "escola_idEscola = ?, " //
					+ "produtor_idProdutor = ?, " //
					+ "produto_idProduto = ?, " //
					+ "quantidade = ? " //
					+ "where idEntregaEscola = ?";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setDate(1, objeto.getDataEntrega());
			sqlParametro.setInt(2, objeto.getIdEscola());
			sqlParametro.setInt(3, objeto.getIdProdutor());
			sqlParametro.setInt(4, objeto.getIdProduto());
			sqlParametro.setDouble(5, objeto.getQuantidade());
			sqlParametro.setInt(6, objeto.getIdEntregaEscola());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from entrega_escola " //
					+ "where idEntregaEscola = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, objeto.getIdEntregaEscola());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				entregaEscola = new EntregaEscola();
				entregaEscola.setIdEntregaEscola(resultado.getInt("idEntregaEscola"));
				entregaEscola.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaEscola.setIdEscola(resultado.getInt("escola_idEscola"));
				entregaEscola.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaEscola.setIdProduto(resultado.getInt("produto_idProduto"));
				entregaEscola.setQuantidade(resultado.getDouble("quantidade"));

				return entregaEscola;
			}
			return null;
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public void excluir(int codigo) throws SQLException {

		conexao = new Conexao().geraConexao();

		try {

			sql = "delete from entrega_escola " //
					+ "where idEntregaEscola = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);
			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public List<EntregaEscola> listar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();
		lista = new ArrayList<EntregaEscola>();

		try {

			sql = "select idEntregaEscola, dataEntrega, quantidade, escola.nome as nomeEscola, produto.nome as nomeProduto, produtor.nome as nomeProdutor, escola.idEscola as idEscola\r\n" + //
					"from entrega_escola\r\n" + //
					"join escola\r\n" + //
					"on escola_idEscola = idEscola\r\n" + //
					"join produto\r\n" + //
					"on produto_idProduto = idProduto\r\n" + //
					"join produtor\r\n" + //
					"on produtor_idProdutor = idProdutor " + //
					"where entrega_escola.escola_idEscola = ?;";

			sqlParametro = conexao.prepareStatement(sql);
			
			sqlParametro.setInt(1, codigo);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				entregaEscola = new EntregaEscola();
				entregaEscola.setIdEntregaEscola(resultado.getInt("idEntregaEscola"));
				entregaEscola.setIdEscola(resultado.getInt("idEscola"));
				entregaEscola.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaEscola.setEscola(resultado.getString("nomeEscola"));
				entregaEscola.setProdutor(resultado.getString("nomeProdutor"));
				entregaEscola.setProduto(resultado.getString("nomeProduto"));
				entregaEscola.setQuantidade(resultado.getDouble("quantidade"));

				lista.add(entregaEscola);
			}
			return lista;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public EntregaEscola buscar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {
			sql = "select * " //
					+ "from entrega_escola " //
					+ "where idEntregaEscola = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				entregaEscola = new EntregaEscola();
				entregaEscola.setIdEntregaEscola(resultado.getInt("idEntregaEscola"));
				entregaEscola.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaEscola.setIdEscola(resultado.getInt("escola_idEscola"));
				entregaEscola.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaEscola.setIdProduto(resultado.getInt("produto_idProduto"));
				entregaEscola.setQuantidade(resultado.getDouble("quantidade"));

				return entregaEscola;
			}

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
