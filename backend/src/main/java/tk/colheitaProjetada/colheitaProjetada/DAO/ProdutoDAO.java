package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.Produto;

public class ProdutoDAO implements IDAO<Produto> {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;
	private List<Produto> lista;
	private Produto produto;

	@Override
	public Produto inserir(Produto objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "insert into produto (nome, orientPlantio, sementePorKg, diasIniProd, diasFinProd, valor) \r\n" + //
					"values (?, ?, ?, ?, ?, ?);";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getOrientPlantio());
			sqlParametro.setDouble(3, objeto.getSementePorKg());
			sqlParametro.setInt(4, objeto.getDiasIniProd());
			sqlParametro.setInt(5, objeto.getDiasFinProd());
			sqlParametro.setDouble(6, objeto.getValor());

			sqlParametro.executeUpdate();

			sql = "select * " + "from produto " + "where idProduto = (" + "select last_insert_id() " + "as idProduto"
					+ ");";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				produto = new Produto();
				produto.setIdProduto(resultado.getInt("idProduto"));
				produto.setNome(resultado.getString("nome"));
				produto.setOrientPlantio(resultado.getString("orientPlantio"));
				produto.setSementePorKg(resultado.getDouble("sementePorKg"));
				produto.setDiasIniProd(resultado.getInt("diasIniProd"));
				produto.setDiasFinProd(resultado.getInt("diasFinProd"));
				produto.setValor(resultado.getDouble("valor"));
			}

			return produto;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Produto salvar(Produto objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "update produto " //
					+ "set " + "nome = ?, " //
					+ "orientPlantio = ?, " //
					+ "sementePorKg = ?, " //
					+ "diasIniProd = ?, " //
					+ "diasFinProd = ?, " //
					+ "valor = ? " //
					+ "where idProduto = ?";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getOrientPlantio());
			sqlParametro.setDouble(3, objeto.getSementePorKg());
			sqlParametro.setInt(4, objeto.getDiasIniProd());
			sqlParametro.setInt(5, objeto.getDiasFinProd());
			sqlParametro.setDouble(6, objeto.getValor());
			sqlParametro.setInt(7, objeto.getIdProduto());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from produto " //
					+ "where idProduto = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, objeto.getIdProduto());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				produto = new Produto();
				produto.setIdProduto(resultado.getInt("idProduto"));
				produto.setNome(resultado.getString("nome"));
				produto.setOrientPlantio(resultado.getString("orientPlantio"));
				produto.setSementePorKg(resultado.getDouble("sementePorKg"));
				produto.setDiasIniProd(resultado.getInt("diasIniProd"));
				produto.setDiasFinProd(resultado.getInt("diasFinProd"));
				produto.setValor(resultado.getDouble("valor"));
			}

			return produto;
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(int codigo) throws SQLException {

		conexao = new Conexao().geraConexao();

		try {

			sql = "delete from produto " //
					+ "where idProduto = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);
			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Produto> listar() throws SQLException {
		conexao = new Conexao().geraConexao();
		lista = new ArrayList<Produto>();

		try {

			sql = "select * " //
					+ "from produto;";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				produto = new Produto();
				produto.setIdProduto(resultado.getInt("idProduto"));
				produto.setNome(resultado.getString("nome"));
				produto.setOrientPlantio(resultado.getString("orientPlantio"));
				produto.setSementePorKg(resultado.getDouble("sementePorKg"));
				produto.setDiasIniProd(resultado.getInt("diasIniProd"));
				produto.setDiasFinProd(resultado.getInt("diasFinProd"));
				produto.setValor(resultado.getDouble("valor"));

				lista.add(produto);
			}
			return lista;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Produto buscar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {
			sql = "select * " //
					+ "from produto " //
					+ "where idProduto = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				produto = new Produto();
				produto.setIdProduto(resultado.getInt("idProduto"));
				produto.setNome(resultado.getString("nome"));
				produto.setOrientPlantio(resultado.getString("orientPlantio"));
				produto.setSementePorKg(resultado.getDouble("sementePorKg"));
				produto.setDiasIniProd(resultado.getInt("diasIniProd"));
				produto.setDiasFinProd(resultado.getInt("diasFinProd"));
				produto.setValor(resultado.getDouble("valor"));
			}

			return produto;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
}
