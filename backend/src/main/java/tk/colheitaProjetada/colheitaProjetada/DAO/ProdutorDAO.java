package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.Cronograma;
import tk.colheitaProjetada.colheitaProjetada.Model.Produtor;

public class ProdutorDAO implements IDAO<Produtor> {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;
	private List<Produtor> lista;
	private Produtor produtor;

	@Override
	public Produtor inserir(Produtor objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "insert into produtor (nome, numDAP, celular, telefone, email, sexo, senha, uf, cidade, bairro, cep, numero, rua, referencia) \r\n"
					+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getNumDAP());
			sqlParametro.setString(3, objeto.getCelular());
			sqlParametro.setString(4, objeto.getTelefone());
			sqlParametro.setString(5, objeto.getEmail());
			sqlParametro.setString(6, objeto.getSexo());
			sqlParametro.setString(7, objeto.getSenha());
			sqlParametro.setString(8, objeto.getUf());
			sqlParametro.setString(9, objeto.getCidade());
			sqlParametro.setString(10, objeto.getBairro());
			sqlParametro.setString(11, objeto.getCep());
			sqlParametro.setString(12, objeto.getNumero());
			sqlParametro.setString(13, objeto.getRua());
			sqlParametro.setString(14, objeto.getReferencia());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from produtor " //
					+ "where idProdutor = (" //
					+ "select last_insert_id() " //
					+ "as idProdutor" //
					+ ");";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				produtor = new Produtor();
				produtor.setIdProdutor(resultado.getInt("idProdutor"));
				produtor.setNome(resultado.getString("nome"));
				produtor.setNumDAP(resultado.getString("numDAP"));
				produtor.setCelular(resultado.getString("celular"));
				produtor.setTelefone(resultado.getString("Telefone"));
				produtor.setEmail(resultado.getString("email"));
				produtor.setSexo(resultado.getString("sexo"));
				produtor.setSenha(resultado.getString("senha"));
				produtor.setUf(resultado.getString("uf"));
				produtor.setCidade(resultado.getString("cidade"));
				produtor.setBairro(resultado.getString("bairro"));
				produtor.setCep(resultado.getString("cep"));
				produtor.setNumero(resultado.getString("numero"));
				produtor.setRua(resultado.getString("rua"));
				produtor.setReferencia(resultado.getString("referencia"));

			}

			return produtor;
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Produtor salvar(Produtor objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "update produtor " //
					+ "set " //
					+ "nome = ?, " //
					+ "numDAP = ?, " //
					+ "celular = ?, " //
					+ "telefone = ?, " //
					+ "email = ?, " //
					+ "sexo = ?, " //
					+ "senha = ?, " //
					+ "uf = ?, " //
					+ "cidade = ?, " //
					+ "bairro = ?, " //
					+ "cep = ?, " //
					+ "numero = ?, " //
					+ "rua = ?, " //
					+ "referencia = ? " //
					+ "where idProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getNumDAP());
			sqlParametro.setString(3, objeto.getCelular());
			sqlParametro.setString(4, objeto.getTelefone());
			sqlParametro.setString(5, objeto.getEmail());
			sqlParametro.setString(6, objeto.getSexo());
			sqlParametro.setString(7, objeto.getSenha());
			sqlParametro.setString(8, objeto.getUf());
			sqlParametro.setString(9, objeto.getCidade());
			sqlParametro.setString(10, objeto.getBairro());
			sqlParametro.setString(11, objeto.getCep());
			sqlParametro.setString(12, objeto.getNumero());
			sqlParametro.setString(13, objeto.getRua());
			sqlParametro.setString(14, objeto.getReferencia());
			sqlParametro.setInt(15, objeto.getIdProdutor());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from produtor " //
					+ "where idProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, objeto.getIdProdutor());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				produtor = new Produtor();
				produtor.setIdProdutor(resultado.getInt("idProdutor"));
				produtor.setNome(resultado.getString("nome"));
				produtor.setNumDAP(resultado.getString("numDAP"));
				produtor.setCelular(resultado.getString("celular"));
				produtor.setTelefone(resultado.getString("Telefone"));
				produtor.setEmail(resultado.getString("email"));
				produtor.setSexo(resultado.getString("sexo"));
				produtor.setSenha(resultado.getString("senha"));
				produtor.setUf(resultado.getString("uf"));
				produtor.setCidade(resultado.getString("cidade"));
				produtor.setBairro(resultado.getString("bairro"));
				produtor.setCep(resultado.getString("cep"));
				produtor.setNumero(resultado.getString("numero"));
				produtor.setRua(resultado.getString("rua"));
				produtor.setReferencia(resultado.getString("referencia"));

				return produtor;
			}

			return null;
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(int codigo) throws SQLException {

		conexao = new Conexao().geraConexao();

		try {

			sql = "delete from produtor " //
					+ "where idProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);
			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Produtor> listar() throws SQLException {
		conexao = new Conexao().geraConexao();
		lista = new ArrayList<Produtor>();

		try {

			sql = "select * " //
					+ "from produtor;";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				produtor = new Produtor();
				produtor.setIdProdutor(resultado.getInt("idProdutor"));
				produtor.setNome(resultado.getString("nome"));
				produtor.setNumDAP(resultado.getString("numDAP"));
				produtor.setCelular(resultado.getString("celular"));
				produtor.setTelefone(resultado.getString("Telefone"));
				produtor.setEmail(resultado.getString("email"));
				produtor.setSexo(resultado.getString("sexo"));
				produtor.setSenha(resultado.getString("senha"));
				produtor.setUf(resultado.getString("uf"));
				produtor.setCidade(resultado.getString("cidade"));
				produtor.setBairro(resultado.getString("bairro"));
				produtor.setCep(resultado.getString("cep"));
				produtor.setNumero(resultado.getString("numero"));
				produtor.setRua(resultado.getString("rua"));
				produtor.setReferencia(resultado.getString("referencia"));

				lista.add(produtor);
			}

			return lista;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<Cronograma> listarCronograma(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();
		List<Cronograma> listCronograma = new ArrayList<Cronograma>();
		
		try {
			
			sql = "select idEntregaProdutor, dataEntrega, produto.nome, escola.nome, quantidade\r\n" + 
					"from entrega_produtor\r\n" + 
					"join escola\r\n" + 
					"on idEscola = escola_idEscola\r\n" + 
					"join produto\r\n" + 
					"on idProduto = produto_idProduto\r\n" + 
					"where produtor_idProdutor = ?\r\n" +
					"order by dataEntrega;";
			
			sqlParametro = conexao.prepareStatement(sql);
			
			sqlParametro.setInt(1, codigo);
			
			resultado = sqlParametro.executeQuery();
			
			while (resultado.next()) {
				Cronograma cronograma = new Cronograma();
				cronograma.setIdCronograma(resultado.getInt("idEntregaProdutor"));
				cronograma.setNomeProduto(resultado.getString("produto.nome"));
				cronograma.setNomeEscola(resultado.getString("escola.nome"));
				cronograma.setQuantidade(resultado.getFloat("quantidade"));
				cronograma.setDataEntrega(resultado.getDate("dataEntrega"));
				
				listCronograma.add(cronograma);
			}
			
			return listCronograma;
			
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Produtor buscar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {
			sql = "select * " //
					+ "from produtor " //
					+ "where idProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				produtor = new Produtor();
				produtor.setIdProdutor(resultado.getInt("idProdutor"));
				produtor.setNome(resultado.getString("nome"));
				produtor.setNumDAP(resultado.getString("numDAP"));
				produtor.setCelular(resultado.getString("celular"));
				produtor.setTelefone(resultado.getString("Telefone"));
				produtor.setEmail(resultado.getString("email"));
				produtor.setSexo(resultado.getString("sexo"));
				produtor.setSenha(resultado.getString("senha"));
				produtor.setUf(resultado.getString("uf"));
				produtor.setCidade(resultado.getString("cidade"));
				produtor.setBairro(resultado.getString("bairro"));
				produtor.setCep(resultado.getString("cep"));
				produtor.setNumero(resultado.getString("numero"));
				produtor.setRua(resultado.getString("rua"));
				produtor.setReferencia(resultado.getString("referencia"));

				return produtor;
			}

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
