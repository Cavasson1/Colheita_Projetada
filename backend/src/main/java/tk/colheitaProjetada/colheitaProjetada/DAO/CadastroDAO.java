package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONException;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.Cadastro;
import tk.colheitaProjetada.colheitaProjetada.Model.Login;

public class CadastroDAO {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;

	public Login cadastro(Cadastro objeto) throws SQLException, JSONException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "insert into produtor (nome, senha, numDAP)\r\n" //
					+ "values (?, ?, ?);";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNome());
			sqlParametro.setString(2, objeto.getSenha());
			sqlParametro.setString(3, objeto.getNumDAP());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from produtor " //
					+ "where idProdutor = (" //
					+ "select last_insert_id() " //
					+ "as idProdutor"//
					+ ");";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				Login login = new Login();
				login.setIdUsuario(resultado.getInt("idProdutor"));
				login.setUsuario(resultado.getString("nome"));
				login.setPermission("user");
				return login;
			}

			return null;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
}
