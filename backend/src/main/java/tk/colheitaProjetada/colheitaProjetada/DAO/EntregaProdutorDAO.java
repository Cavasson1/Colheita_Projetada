package tk.colheitaProjetada.colheitaProjetada.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tk.colheitaProjetada.colheitaProjetada.Config.Conexao;
import tk.colheitaProjetada.colheitaProjetada.Model.EntregaProdutor;

public class EntregaProdutorDAO implements IDAO<EntregaProdutor> {
	private Connection conexao;
	private PreparedStatement sqlParametro = null;
	private ResultSet resultado = null;
	private String sql;
	private List<EntregaProdutor> lista;
	private EntregaProdutor entregaProdutor;

	@Override
	public EntregaProdutor inserir(EntregaProdutor objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "insert into entrega_produtor (dataEntrega, escola_idEscola, produtor_idProdutor, produto_idProduto, quantidade) \r\n"
					+ "values (?, ?, ?, ?, ?);";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setDate(1, objeto.getDataEntrega());
			sqlParametro.setInt(2, objeto.getIdEscola());
			sqlParametro.setInt(3, objeto.getIdProdutor());
			sqlParametro.setInt(4, objeto.getIdProduto());
			sqlParametro.setDouble(5, objeto.getQuantidade());

			sqlParametro.executeUpdate();

			sql = "select * " + "from entrega_produtor " + "where idEntregaProdutor = (" + "select last_insert_id() "
					+ "as idEntregaProdutor" + ");";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				entregaProdutor = new EntregaProdutor();
				entregaProdutor.setIdEntregaProdutor(resultado.getInt("idEntregaProdutor"));
				entregaProdutor.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaProdutor.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaProdutor.setIdEscola(resultado.getInt("escola_idEscola"));
				entregaProdutor.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaProdutor.setIdProduto(resultado.getInt("produto_idProduto"));
				entregaProdutor.setQuantidade(resultado.getDouble("quantidade"));
			}

			return entregaProdutor;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public EntregaProdutor salvar(EntregaProdutor objeto) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "update entrega_produtor " //
					+ "set " + "dataEntrega = ?, " //
					+ "escola_idEscola = ?, " //
					+ "produtor_idProdutor = ?, " //
					+ "produto_idProduto = ?, " //
					+ "quantidade = ? " //
					+ "where idEntregaProdutor = ?";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setDate(1, objeto.getDataEntrega());
			sqlParametro.setInt(2, objeto.getIdEscola());
			sqlParametro.setInt(3, objeto.getIdProdutor());
			sqlParametro.setInt(4, objeto.getIdProduto());
			sqlParametro.setDouble(5, objeto.getQuantidade());
			sqlParametro.setInt(6, objeto.getIdEntregaProdutor());

			sqlParametro.executeUpdate();

			sql = "select * " //
					+ "from entrega_produtor " //
					+ "where idEntregaProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, objeto.getIdEntregaProdutor());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				entregaProdutor = new EntregaProdutor();
				entregaProdutor.setIdEntregaProdutor(resultado.getInt("idEntregaProdutor"));
				entregaProdutor.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaProdutor.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaProdutor.setIdEscola(resultado.getInt("escola_idEscola"));
				entregaProdutor.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaProdutor.setIdProduto(resultado.getInt("produto_idProduto"));
				entregaProdutor.setQuantidade(resultado.getDouble("quantidade"));

				return entregaProdutor;
			}

			return null;
		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(int codigo) throws SQLException {

		conexao = new Conexao().geraConexao();

		try {

			sql = "delete from entrega_produtor " //
					+ "where idEntregaProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);
			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public List<EntregaProdutor> listar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();
		lista = new ArrayList<EntregaProdutor>();

		try {

			sql = "select idEntregaProdutor, dataEntrega, quantidade, escola.nome, produto.nome, produtor.nome, produtor.idProdutor\r\n" + 
					"from entrega_produtor\r\n" + 
					"join escola\r\n" + 
					"on escola_idEscola = idEscola\r\n" + 
					"join produto\r\n" + 
					"on produto_idProduto = idProduto\r\n" + 
					"join produtor\r\n" + 
					"on produtor_idProdutor = idProdutor\r\n" + 
					"where produtor_idProdutor = ?\r\n" + 
					"order by dataEntrega;"; //

			sqlParametro = conexao.prepareStatement(sql);
			
			sqlParametro.setInt(1, codigo);
			
			resultado = sqlParametro.executeQuery();

			while (resultado.next()) {
				entregaProdutor = new EntregaProdutor();
				entregaProdutor.setIdEntregaProdutor(resultado.getInt("idEntregaProdutor"));
				entregaProdutor.setIdProdutor(resultado.getInt("produtor.idProdutor"));
				entregaProdutor.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaProdutor.setEscola(resultado.getString("escola.nome"));
				entregaProdutor.setProdutor(resultado.getString("produtor.nome"));
				entregaProdutor.setProduto(resultado.getString("produto.nome"));
				entregaProdutor.setQuantidade(resultado.getDouble("quantidade"));
				

				lista.add(entregaProdutor);
			}
			return lista;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public List<EntregaProdutor> listar() throws SQLException {
		return null;
	}

	@Override
	public EntregaProdutor buscar(int codigo) throws SQLException {
		conexao = new Conexao().geraConexao();

		try {
			sql = "select * " //
					+ "from entrega_produtor " //
					+ "where idEntregaProdutor = ?;";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setInt(1, codigo);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				entregaProdutor = new EntregaProdutor();
				entregaProdutor.setIdEntregaProdutor(resultado.getInt("idEntregaProdutor"));
				entregaProdutor.setDataEntrega(resultado.getDate("dataEntrega"));
				entregaProdutor.setIdEscola(resultado.getInt("escola_idEscola"));
				entregaProdutor.setIdProdutor(resultado.getInt("produtor_idProdutor"));
				entregaProdutor.setIdProduto(resultado.getInt("produto_idProduto"));
				entregaProdutor.setQuantidade(resultado.getDouble("quantidade"));

				return entregaProdutor;
			}

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
