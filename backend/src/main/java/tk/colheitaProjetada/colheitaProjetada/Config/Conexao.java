package tk.colheitaProjetada.colheitaProjetada.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	public Connection geraConexao() {
		Connection conexao = null;

		try {
			
			String url = "jdbc:mysql://localhost/colheita projetada?useSSL=false&useTimezone=true&serverTimezone=UTC";
			String usuario = "root";
			String senha = "root";
			conexao = DriverManager.getConnection(url, usuario, senha);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return conexao;
	}
}
