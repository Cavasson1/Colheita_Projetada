import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Entrega } from './entrega.model';
import { EntregaService } from './entrega.service';

import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-entrega-lista-escola',
  templateUrl: './entrega-lista-escola.component.html'
})
export class EntregaListaEscolaComponent implements OnInit {

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Código', name: 'idEntregaEscola' },
    { title: 'Produto', name: 'produto' },
    { title: 'Produtor', name: 'produtor' },
  ];
  public page = 1;
  public itemsPerPage = 10;
  public maxSize = 5;
  public numPages = 1;
  public length = 5;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  private data;

  private escolaNome;
  private idEscola;

  public constructor(
    private entregaService: EntregaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private _service: NotificationsService
  ) {
    // this.length = this.data.length;
  }

  public ngOnInit(): void {
    // verifica se está logado
    if (localStorage.getItem('user')) {
      // verifica a permissão
      if (JSON.parse(localStorage.getItem('user')).permission === 'adm') {
        this.activatedRoute.params.forEach((params: Params) => {
          const id: number = +params['id'];
          if (id) {
            this.idEscola = id;
            this.entregaService.findAllEscola(id)
              .then((data: Entrega[]) => {
                this.data = data;
                this.escolaNome = data[0].escola;
                this.onChangeTable(this.config);
              })
              .catch(() => {
                console.log('Vish, deu merda, ao tentar recupera os dados');
              });
          }
        });

      } else {
        this.router.navigateByUrl('home');
        this._service.error('Erro', 'Você não tem permissão para acessar essa página!', this.configGritter);
      }
    } else {
      this.router.navigateByUrl('login');
      this._service.error('Erro', 'É necessário estar logado para acessar essa página!', this.configGritter);
    }
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          // return item[column.name].match(column.filtering.filterString);
          return (item[column.name] === column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name]) {
          if (item[column.name].toString().match(this.config.filtering.filterString)) {
            flag = true;
          }
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    const url = 'entrega/escola/save';
    this.router.navigateByUrl(`${url}/${data.row.idEntregaEscola}`);
  }

  goBack(): void {
    this.router.navigateByUrl('/entrega');
  }

  novo(): void {
    this.router.navigateByUrl('entrega/escola/new/' + this.idEscola);
  }
}
