import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BsDatepickerModule } from 'ngx-bootstrap';

import { IconeModule } from '.././icone/icone.module';
import { MenuModule } from '.././menu/menu.module';
import { EntregaDetalheEscolaComponent } from './entrega-detalhe-escola.component';
import { EntregaDetalheProdutorComponent } from './entrega-detalhe-produtor.component';
import { EntregaListaEscolaComponent } from './entrega-lista-escola.component';
import { EntregaListaProdutorComponent } from './entrega-lista-produtor.component';
import { EntregaListaComponent } from './entrega-lista.component';
import { EntregaRoutingModule } from './entrega-routing.module';
import { EntregaService } from './entrega.service';

@NgModule({
  imports: [
    CommonModule,
    EntregaRoutingModule,
    FormsModule,
    MenuModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    TabsModule,
    TextMaskModule,
    IconeModule,
    NgbModule,
    BsDatepickerModule
  ],
  declarations: [
    EntregaDetalheEscolaComponent,
    EntregaDetalheProdutorComponent,
    EntregaListaComponent,
    EntregaListaEscolaComponent,
    EntregaListaProdutorComponent
  ],
  exports: [
    EntregaListaComponent
  ],
  providers: [
    EntregaService,
    DatePipe
  ]
})
export class EntregaModule {}
