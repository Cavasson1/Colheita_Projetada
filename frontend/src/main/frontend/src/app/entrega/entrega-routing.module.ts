import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EntregaDetalheEscolaComponent } from './entrega-detalhe-escola.component';
import { EntregaDetalheProdutorComponent } from './entrega-detalhe-produtor.component';
import { EntregaListaEscolaComponent } from './entrega-lista-escola.component';
import { EntregaListaProdutorComponent } from './entrega-lista-produtor.component';

const routes: Routes = [{
  path: 'entrega/escola/:id',
  component: EntregaListaEscolaComponent
}, {
  path: 'entrega/produtor/:id',
  component: EntregaListaProdutorComponent
}, {
  path: 'entrega/escola/new/:idEsc',
  component: EntregaDetalheEscolaComponent
}, {
  path: 'entrega/escola/save/:id',
  component: EntregaDetalheEscolaComponent
}, {
  path: 'entrega/produtor/new/:idProd',
  component: EntregaDetalheProdutorComponent
}, {
  path: 'entrega/produtor/save/:id',
  component: EntregaDetalheProdutorComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EntregaRoutingModule { }
