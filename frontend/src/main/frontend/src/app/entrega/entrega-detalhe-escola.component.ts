import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location, DatePipe } from '@angular/common';

import { defineLocale } from 'ngx-bootstrap/bs-moment';
import { ptBr } from 'ngx-bootstrap/locale';
defineLocale('pt-br', ptBr);

import { Entrega } from './entrega.model';
import { EntregaService } from './entrega.service';

import createNumberMask from 'text-mask-addons/dist/createNumberMask';

import { NotificationsService } from 'angular2-notifications';

import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { listLocales } from 'ngx-bootstrap/bs-moment';

const now = new Date();

@Component({
  selector: 'app-entrega-detalhe-escola',
  templateUrl: './entrega-detalhe-escola.component.html'
})
export class EntregaDetalheEscolaComponent implements OnInit {

  locale = 'pt-br';
  bsConfig: Partial<BsDatepickerConfig>;

  bsValue: Date;
  bsRangeValue: any = [new Date(2017, 7, 4), new Date(2017, 7, 20)];

  model: NgbDateStruct;
  date: { year: number, month: number };

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  private editMode = false;
  entrega: Entrega;

  public produtos;
  public produtores;

  public dataMask = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public qtdMask = createNumberMask({
    prefix: '',
    suffix: ' Kg',
    allowDecimal: false,
    decimalSymbol: ',',
    thousandsSeparatorSymbol: '.',
    integerLimit: 6
  });

  constructor(
    private entregaService: EntregaService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.bsConfig = Object.assign({}, { locale: this.locale });
    this.entrega = new Entrega();

    // verifica se está logado
    if (localStorage.getItem('user')) {
      // verifica a permissão
      if (JSON.parse(localStorage.getItem('user')).permission === 'adm') {

        this.entregaService.getDados()
          .then((data) => {
            this.produtos = data.produto;
            this.produtores = data.produtor;
          })
          .catch(() => {
            console.log('Vish, deu merda ao tentar recupera os dados');
            this._service.error('Erro', 'Ops, ocorreu um erro ao recuperar os dados!', this.configGritter);
          });

        this.activatedRoute.params.forEach((params: Params) => {
          const id: number = +params['id'];
          const user: number = +params['idEsc'];

          if (id) {
            this.editMode = true;
            this.entregaService.findEscola(id)
              .then((entrega: Entrega) => {
                this.entrega = entrega;
              });
          } else if (user) {
            this.entrega.idEscola = user;
          }
        });
      } else {
        this.router.navigateByUrl('home');
        this._service.error('Erro', 'Você não tem permissão para acessar essa página!', this.configGritter);
      }
    } else {
      this.router.navigateByUrl('login');
      this._service.error('Erro', 'É necessário estar logado para acessar essa página!', this.configGritter);
    }
  }

  onSubmit(): void {
    let promise;
    if (this.editMode) {
      promise = this.entregaService.updateEsc(this.entrega);
      this.entrega = new Entrega();
    } else {
      promise = this.entregaService.createEsc(this.entrega);
    }

    promise.then(entrega => {
      const url = 'entrega/escola/save';
      this.router.navigateByUrl(`${url}/${entrega.idEntregaEscola}`);
      this.entrega = entrega;
      this._service.success('Sucesso', 'Operação executada com sucesso!', this.configGritter);
    }).catch((err) => {
      console.log(err);
      this._service.error('Erro', 'Ops, algo deu errado ao salvar o registro!', this.configGritter);
    });
  }

  goBack(): void {
    this.router.navigateByUrl('/entrega');
  }
}
