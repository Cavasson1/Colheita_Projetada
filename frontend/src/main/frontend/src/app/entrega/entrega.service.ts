import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { Entrega } from './entrega.model';
import { ServiceInterface } from './../interface/service.interface';

import * as moment from 'moment';
import 'moment/locale/pt-br';

@Injectable()
export class EntregaService {

  private apiUrl = 'http://localhost:8080';
  private headers: Headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(
    private http: Http
  ) {
    moment.locale('pt-BR');
  }

  findAll(): Promise<Entrega[]> {
    return this.http
      .get(this.apiUrl + '/app/entrega/listar')
      .toPromise()
      .then(response => response.json() as Entrega[])
      .catch(this.showError);
  }

  findAllEscola(id: number): Promise<Entrega[]> {
    return this.http
      .get(this.apiUrl + '/app/entregaEscola/listarTodos/' + id)
      .toPromise()
      .then(response => response.json().data as Entrega[])
      .catch(this.showError);
  }

  findAllProdutor(id: number): Promise<Entrega[]> {
    return this.http
      .get(this.apiUrl + '/app/entregaProdutor/listarTodos/' + id)
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  findEscola(id: number): Promise<Entrega> {
    return this.http
      .get(this.apiUrl + '/app/entregaEscola/listar/' + id)
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  findProdutor(id: number): Promise<Entrega> {
    return this.http
      .get(this.apiUrl + '/app/entregaProdutor/listar/' + id)
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  find(id: number): Promise<Entrega> {
    return null;
  }

  createProd(produtor: Entrega): Promise<Entrega> {
    return this.http
      .post(this.apiUrl + '/app/entregaProdutor/salvar', JSON.stringify(this.normalizeModel(produtor)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  updateProd(produtor: Entrega): Promise<Entrega> {
    return this.http
      .put(this.apiUrl + '/app/entregaProdutor/salvar', JSON.stringify(this.normalizeModel(produtor)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  createEsc(escola: Entrega): Promise<Entrega> {
    return this.http
      .post(this.apiUrl + '/app/entregaEscola/salvar', JSON.stringify(this.normalizeModel(escola)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  updateEsc(escola: Entrega): Promise<Entrega> {
    return this.http
      .put(this.apiUrl + '/app/entregaEscola/salvar', JSON.stringify(this.normalizeModel(escola)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => {
        let obj = response.json().data as Entrega;
        obj.dataEntrega = this.formataData(obj.dataEntrega);
        return obj;
      })
      .catch(this.showError);
  }

  deleteProd(produtor: Entrega): Promise<Entrega> {

    return null;
  }

  private formataData(date) {
    return moment(date).add(1, 'd').toString();
  }

  getDados(): Promise<any> {
    return this.http
      .get(this.apiUrl + '/app/entrega/dados')
      .toPromise()
      .then(response => response.json().data)
      .catch(this.showError);
  }


  private normalizeModel(entrega: Entrega) {

    function numberOnly(value) {
      if (typeof value === 'string') {
        return value.replace(/[^0-9]/g, '');
      }
      return value;
    }

    return {
      idEntregaEscola: +entrega.idEntregaEscola || null,
      idEntregaProdutor: +entrega.idEntregaProdutor || null,
      idProduto: +entrega.idProduto || null,
      idEscola: +entrega.idEscola || null,
      idProdutor: +entrega.idProdutor || null,
      dataEntrega: moment(entrega.dataEntrega).format('YYYY-MM-DD'),
      quantidade: numberOnly(entrega.quantidade),
    };
  }


  private showError(err: any): Promise<any> {
    console.log('Erro exibindo da classe Produtor.service: ', err);
    return Promise.reject(err.message || err);
  }

}

