import { Produto } from '.././produto/produto.model';

export class Entrega {

  public idEntrega: number;
  public idEntregaEscola: number;
  public idEntregaProdutor: number;
  public escola: string;
  public produtor: string;
  public produtos: Produto[];
  public dataEntrega;
  public idProduto: number;
  public idEscola: number;
  public idProdutor: number;
  public quantidade;

  constructor() { }
}
