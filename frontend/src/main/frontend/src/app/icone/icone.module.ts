import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { IconeComponent } from './icone.component';
import { IconeRoutingModule } from './icone-routing.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    IconeRoutingModule,
    NgbModule
  ],
  declarations: [
    IconeComponent
  ],
  exports: [
    IconeComponent
  ]
})
export class IconeModule {}
