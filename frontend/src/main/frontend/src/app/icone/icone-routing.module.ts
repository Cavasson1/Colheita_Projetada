import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '.././home/home.component';
import { EntregaListaComponent } from '.././entrega/entrega-lista.component';
import { EscolaListaComponent } from '.././escola/escola-lista.component';
import { ProdutoListaComponent } from '.././produto/produto-lista.component';
import { ProdutorListaComponent } from '.././produtor/produtor-lista.component';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class IconeRoutingModule { }
