import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-icone',
  templateUrl: './icone.component.html'
})
export class IconeComponent implements OnInit {

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  public exibirPop;

  private rota: string;
  public usuario: string;

  constructor(
    private router: Router,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.rota = this.router.url;

    if (localStorage.getItem('user')) {
      this.usuario = JSON.parse(localStorage.getItem('user')).name;
    }
  }

  logout(): void {
    localStorage.clear();
    this.router.navigateByUrl('/login');
    this._service.info('Informação', 'A sessão foi encerrada!', this.configGritter);
  }

}
