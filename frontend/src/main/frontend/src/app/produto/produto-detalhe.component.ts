import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Produto } from './produto.model';
import { ProdutoService } from './produto.service';

import { NotificationsService } from 'angular2-notifications';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

@Component({
  selector: 'app-produto-detalhe',
  templateUrl: './produto-detalhe.component.html'
})
export class ProdutoDetalheComponent implements OnInit {

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  private editMode = false;
  produto: Produto;

  public diasMask = createNumberMask({
    prefix: '',
    suffix: '',
    thousandsSeparatorSymbol: '.',
    integerLimit: 6
  });

  public sementeMask = createNumberMask({
    prefix: '',
    suffix: '',
    allowDecimal: true,
    decimalSymbol: ',',
    thousandsSeparatorSymbol: '.',
    integerLimit: 6
  });

  public precoMask = createNumberMask({
    prefix: 'R$ ',
    suffix: '',
    allowDecimal: true,
    decimalSymbol: ',',
    thousandsSeparatorSymbol: '.',
    integerLimit: 6
  });

  private id: number;

  constructor(
    private produtoService: ProdutoService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.produto = new Produto();
    // verifica se está logado
    if (localStorage.getItem('user')) {
      // verifica a permissão
      if (JSON.parse(localStorage.getItem('user')).permission === 'adm') {

        this.activatedRoute.params.forEach((params: Params) => {
          this.id = +params['id'];
          if (this.id) {
            this.editMode = true;
            this._buscar();
          }
        });
      } else {
        this.router.navigateByUrl('home');
        this._service.error('Erro', 'Você não tem permissão para acessar essa página!', this.configGritter);
      }
    } else {
      this.router.navigateByUrl('login');
      this._service.error('Erro', 'É necessário estar logado para acessar essa página!', this.configGritter);
    }
  }

  onSubmit(): void {
    let promise;

    if (this.editMode) {
      promise = this.produtoService.update(this.produto);
    } else {
      promise = this.produtoService.create(this.produto);
    }

    promise.then(produto => {
      this._service.success('Sucesso', 'Operação Executada com Sucesso', this.configGritter);

      const url = 'produto/save';
      this.router.navigateByUrl(`${url}/${produto.idProduto}`);
      this.produto = produto;
    })
      .catch((err) => {
        console.log(err);
        this._service.error('Erro', 'Ops, algo deu errado ao salvar o registro!', this.configGritter);
      });
  }

  deletar(): void {
    const promise = this.produtoService.delete(this.produto);

    promise.then(produto => {
      this._service.success('Sucesso', 'Operação Executada com Sucesso', this.configGritter);
      this.router.navigateByUrl('/produto');
      this._buscar();
    });
  }

  _buscar(): void {
    this.produtoService.find(this.id)
    .then((produto: Produto) => {
      this.produto = produto;
    })
    .catch((err) => {
      console.error(err);
      this._service.error('Erro', 'Ops, ocorreu um erro ao buscar o registro!', this.configGritter);
    });
  }

  goBack(): void {
    this.router.navigateByUrl('/produto');
  }
}
