import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { Produto } from './produto.model';
import { ServiceInterface } from './../interface/service.interface';

@Injectable()
export class ProdutoService implements ServiceInterface<Produto> {

  private api = 'http://localhost:8080';
  private headers: Headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(
    private http: Http
  ) { }

  findAll(): Promise<Produto[]> {
    return this.http
      .get(this.api + '/app/produto/listar')
      .toPromise()
      .then(response => response.json().data as Produto[])
      .catch(this.showError);
  }

  find(id: number): Promise<Produto> {
    return this.http
      .get(this.api + '/app/produto/listar/' + id)
      .toPromise()
      .then(response => response.json().data as Produto[])
      .catch(this.showError);
  }

  create(produto: Produto): Promise<Produto> {
    return this.http
      .post(this.api + '/app/produto/salvar', JSON.stringify(this.normalizeModel(produto)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => response.json().data as Produto)
      .catch(this.showError);
  }

  update(produto: Produto): Promise<Produto> {
    return this.http
      .put(this.api + '/app/produto/salvar', JSON.stringify(this.normalizeModel(produto)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => response.json().data as Produto)
      .catch(this.showError);
  }

  delete(produto: Produto): Promise<Produto> {
    return this.http
    .delete(this.api + '/app/produto/deletar/' + produto.idProduto, { headers: this.headers })
    .toPromise()
    .then((response: Response) => response.json().data as Produto)
    .catch(this.showError);
  }

  private showError(err: any): Promise<any> {
    return Promise.reject(err.message || err);
  }

  private normalizeModel(produto: Produto) {

    function numberOnly(value): number {
      if (typeof value === 'string') {
        return parseFloat(value.replace(/[^0-9]/g, ''));
      }
      return value;
    }

    return {
      idProduto: +produto.idProduto,
      nome: produto.nome,
      orientPlantio: produto.orientPlantio,
      sementePorKg: +produto.sementePorKg,
      diasIniProd: +produto.diasIniProd,
      diasFinProd: +produto.diasFinProd,
      valor: numberOnly(produto.valor)
    };
  }










  // getProdutosSlowly(): Promise<Produto[]> {
  //   return new Promise((success, error) => {
  //     setTimeout(success, 3000);
  //   })
  //     .then(() => {
  //       console.log('primeiro then');
  //       return 'Ola mundo!';
  //     })
  //     .then((param) => {
  //       console.log('segundo then');
  //       console.log(param);

  //       return new Promise((success2, error2) => {

  //         setTimeout(function () {
  //           console.log('continuaremos depois de 2 segundos...');
  //           setTimeout(() => {
  //             console.log('HA HA');
  //             success2(); // se não chamar essa função, não executaria o then que esta logo abaixo
  //           }, 2000);
  //         }, 2000);

  //       });
  //     })
  //     .then(() => {
  //       console.log('terceiro then');
  //       return this.findAll();
  //     });
  // }


  /**
   * Exemplo de uso de promises:
   * - Primeiro Then:
   * Busca o usuário, e recupera o id
   * - segundo then:
   * Faz outra chamada assincrona ao servidor, que buscaria dados adicionais no servidor, por exemplo,
   * dados de acesso
   * - terceiro then:
   * Implementaria a logica: Se deixa o usuário acessar a determinada pagina solicitada ou n
   *
   * Obs.: Pode ter outra promisse detro de outro then, dessa forma, o segundo, da cadeia principal,
   * só executa quando o primeiro terminar completamente
   */

}
