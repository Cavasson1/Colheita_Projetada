import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProdutoDetalheComponent } from './produto-detalhe.component';

const produtoRoutes: Routes = [
    {
        path: 'produto/save',
        component: ProdutoDetalheComponent
    },
    {
        path: 'produto/save/:id',
        component: ProdutoDetalheComponent
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(produtoRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProdutoRoutingModule { }
