import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

import { IconeModule } from '.././icone/icone.module';
import { MenuModule } from '.././menu/menu.module';
import { ProdutoDetalheComponent } from './produto-detalhe.component';
import { ProdutoListaComponent } from './produto-lista.component';
import { ProdutoRoutingModule } from './produto-routing.module';
import { ProdutoService } from './produto.service';

@NgModule({
    imports: [
        CommonModule,
        ProdutoRoutingModule,
        FormsModule,
        MenuModule,
        Ng2TableModule,
        PaginationModule.forRoot(),
        TabsModule,
        TextMaskModule,
        IconeModule
    ],
    declarations: [
        ProdutoListaComponent,
        ProdutoDetalheComponent
    ],
    exports: [
        ProdutoListaComponent
    ],
    providers: [
        ProdutoService
    ]
})
export class ProdutoModule { }
