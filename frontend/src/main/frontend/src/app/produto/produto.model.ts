
export class Produto {

    public idProduto;
    public nome;
    public orientPlantio;
    public sementePorKg;
    public diasIniProd;
    public diasFinProd;
    public valor;

    constructor() { }
}
