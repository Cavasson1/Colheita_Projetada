import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

import { IconeModule } from '.././icone/icone.module';
import { MenuModule } from '.././menu/menu.module';
import { ProdutorDetalheComponent } from './produtor-detalhe.component';
import { ProdutorListaComponent } from './produtor-lista.component';
import { ProdutorRoutingModule } from './produtor-routing.module';
import { ProdutorService } from './produtor.service';

@NgModule({
  imports: [
    CommonModule,
    ProdutorRoutingModule,
    FormsModule,
    MenuModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    TabsModule,
    TextMaskModule,
    IconeModule
  ],
  declarations: [
    ProdutorListaComponent,
    ProdutorDetalheComponent
  ],
  exports: [
    ProdutorListaComponent
  ],
  providers: [
    ProdutorService
  ]
})
export class ProdutorModule { }
