import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProdutorDetalheComponent } from './produtor-detalhe.component';

const routes: Routes = [{
  path: 'produtor/save',
  component: ProdutorDetalheComponent
}, {
  path: 'produtor/save/:id',
  component: ProdutorDetalheComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProdutorRoutingModule { }
