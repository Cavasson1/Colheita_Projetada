import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Produtor } from './produtor.model';
import { ProdutorService } from './produtor.service';

import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import emailMask from 'text-mask-addons/dist/emailMask';

import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-produtor-detalhe',
  templateUrl: './produtor-detalhe.component.html'
})
export class ProdutorDetalheComponent implements OnInit {

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  private editMode = false;
  produtor: Produtor;

  public idadeMask = [/[0-9]/, /\d/, /\d/];
  public maskCelular = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskTelefone = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCep = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  public dapMask = [/[A-Z]/i, /[A-Z]/i, /[A-Z]/i, ' ', /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ',
    /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/];
  public emailMask = emailMask;
  public numeroMask = createNumberMask({
    prefix: '',
    suffix: '',
    thousandsSeparatorSymbol: '.',
    integerLimit: 9
  });

  private exibeExcluir;
  private id: number;

  constructor(
    private produtorService: ProdutorService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.produtor = new Produtor();
    // verifica se está logado
    if (localStorage.getItem('user')) {
      // verifica a permissão
      if (JSON.parse(localStorage.getItem('user')).permission === 'adm') {

        this.activatedRoute.params.forEach((params: Params) => {
          this.id = +params['id'];
          if (this.id) {
            this.editMode = true;
            this._buscar();
          }
        });
      } else {
        this.router.navigateByUrl('home');
        this._service.error('Erro', 'Você não tem permissão para acessar essa página!', this.configGritter);
      }
    } else {
      this.router.navigateByUrl('login');
      this._service.error('Erro', 'É necessário estar logado para acessar essa página!', this.configGritter);
    }
  }

  onSubmit(): void {
    let promise;

    if (this.editMode) {
      promise = this.produtorService.update(this.produtor);
    } else {
      promise = this.produtorService.create(this.produtor);
    }

    promise.then(produtor => {
      this._service.success('Sucesso', 'Operação Executada com Sucesso', this.configGritter);

      const url = 'produtor/save';
      this.router.navigateByUrl(`${url}/${produtor.idProdutor}`);
      this.produtor = produtor;
    })
      .catch((err) => {
        console.log(err);
        this._service.error('Erro', 'Ops, algo deu errado ao salvar o registro!', this.configGritter);
      });

  }

  deletar(): void {
    const promise = this.produtorService.delete(this.produtor);

    promise.then(produtor => {
      this._service.success('Sucesso', 'Operação Executada com Sucesso', this.configGritter);
      this.router.navigateByUrl('/produtor');
      this._buscar();
    });
  }

  _buscar(): void {
    this.produtorService.find(this.id)
    .then((produtor: Produtor) => {
      this.produtor = produtor;
    })
    .catch((err) => {
      console.error(err);
      this._service.error('Erro', 'Ops, ocorreu um erro ao buscar o registro!', this.configGritter);
    });
  }

  novo(): void {
    this.router.navigateByUrl('entrega/produtor/new/' + this.produtor.idProdutor);
  }

  goBack(): void {
    this.router.navigateByUrl('/produtor');
  }
}
