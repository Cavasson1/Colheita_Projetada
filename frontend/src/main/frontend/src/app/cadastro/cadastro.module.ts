import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CadastroComponent } from './cadastro.component';
import { CadastroRoutingModule } from './cadastro-routing.module';
import { LoginModule } from '.././login/login.module';
import { CadastroService } from './cadastro.service';
import { HomeModule } from '.././home/home.module';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    CadastroRoutingModule,
    FormsModule,
    HomeModule,
    TextMaskModule
  ],
  declarations: [
    CadastroComponent
  ],
  exports: [
    CadastroComponent
  ],
  providers: [
    CadastroService
]
})
export class CadastroModule { }
