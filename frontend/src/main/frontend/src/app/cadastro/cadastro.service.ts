import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { Cadastro } from './cadastro.model';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CadastroService {

    private api: string = 'http://localhost:8080';

    private url: string = '/app/cadastro';
    private headers: Headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor(
        private http: Http
    ) { }

    cadastro(cadastro): Promise<any> {
        return this.http
            .post(this.api + this.url, JSON.stringify(this.normalizeModel(cadastro)), { headers: this.headers })
            .toPromise()
            .then(response => response.json().data)
            .catch(this.showError);
    }

    showError(err: any): Promise<any> {
        return Promise.reject(err.message || err);
    }

    private normalizeModel(cadastro: Cadastro) {
        function removerEspeciais(value) {
            if (typeof value === 'string') {
                return value.replace(/[^a-zA-Z1-9]/g, '');
            }
        }
        return {
            nome: cadastro.nome,
            senha: cadastro.senha,
            numDAP: removerEspeciais(cadastro.numDAP),
        };
    }
}
