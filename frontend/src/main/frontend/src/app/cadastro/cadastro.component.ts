import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Cadastro } from './cadastro.model';
import { CadastroService } from './cadastro.service';

import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-cadastro-component',
  templateUrl: './cadastro.component.html'
})
export class CadastroComponent implements OnInit {
  private cadastro: Cadastro;

  public dapMask = [/[A-Z]/i, /[A-Z]/i, /[A-Z]/i, ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/,
    /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  constructor(
    private router: Router,
    private cadastroService: CadastroService,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.cadastro = new Cadastro();
    localStorage.clear();
  }

  cadastrar(): void {
    this.cadastroService.cadastro(this.cadastro)
      .then((res) => {

        localStorage.setItem('user', JSON.stringify({ 'idProdutor': res.idUsuario, 'name': res.usuario, 'permission': res.permission }));

        this.router.navigateByUrl('/home');
        this._service.success('Sucesso', 'Operação executada com sucesso!', this.configGritter);
      })
      .catch((err) => {
        console.error(err || err);
        this._service.error('Erro', 'Ops, ocorreu um erro ao realizar o cadastro!', this.configGritter);
      });
  }
}
