import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { Produtor } from './produtorDetalhe.model';
import { ServiceInterface } from './../interface/service.interface';

@Injectable()
export class ProdutorDetalheService implements ServiceInterface<Produtor> {

  private api = 'http://localhost:8080';
  private headers: Headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(
    private http: Http
  ) { }

  findAll(): Promise<Produtor[]> {
    return null;
  }

  find(): Promise<Produtor> {
    return this.http
      .get(this.api + '/app/produtor/listar/' + JSON.parse(localStorage.getItem('user')).idProdutor)
      .toPromise()
      .then(response => response.json().data as Produtor[])
      .catch(this.showError);
  }

  create(produtor: Produtor): Promise<Produtor> {
    return this.http
      .post(this.api + '/app/produtor/salvar', JSON.stringify(this.normalizeModel(produtor)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => response.json().data as Produtor)
      .catch(this.showError);
  }

  update(produtor: Produtor): Promise<Produtor> {
    return this.http
      .put(this.api + '/app/produtor/salvar', JSON.stringify(this.normalizeModel(produtor)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => response.json().data as Produtor)
      .catch(this.showError);
  }

  delete(produtor: Produtor): Promise<Produtor> {

    return null;
  }




  private showError(err: any): Promise<any> {
    return Promise.reject(err.message || err);
  }

  private normalizeModel(produtor: Produtor) {

    function numberOnly(value) {
      if (typeof value === 'string') {
        return value.replace(/[^0-9]/g, '');
      }
    }

    function removerEspeciais(value) {
      if (typeof value === 'string') {
        return value.replace(/[^a-zA-Z1-9]/g, '');
      }
    }

    return {
      idProdutor: +produtor.idProdutor,
      nome: produtor.nome,
      numDAP: removerEspeciais(produtor.numDAP),
      celular: numberOnly(produtor.celular),
      telefone: numberOnly(produtor.telefone),
      email: produtor.email,
      sexo: produtor.sexo,
      senha: produtor.senha,
      uf: produtor.uf,
      cidade: produtor.cidade,
      bairro: produtor.bairro,
      cep: numberOnly(produtor.cep),
      numero: numberOnly(produtor.numero),
      rua: produtor.rua,
      referencia: produtor.referencia
    };
  }
}

