
export class Produtor {

  public idProdutor: number;
  public nome: string;
  public numDAP: string;
  public celular: string;
  public telefone: string;
  public email: string;
  public sexo: string;
  public senha: string;
  public uf: string;
  public cidade: string;
  public bairro: string;
  public cep: string;
  public numero: string;
  public rua: string;
  public referencia: string;

  constructor() { }
}
