import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { Cronograma } from './cronograma.model';
import { ServiceInterface } from './../interface/service.interface';

import * as moment from 'moment';
import 'moment/locale/pt-br';

@Injectable()
export class CronogramaService {

  private api = 'http://localhost:8080';
  private headers: Headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(
    private http: Http
  ) { }

  find(id: number): Promise<Cronograma> {
    return this.http
      .get(this.api + '/app/produtor/cronograma/' + id)
      .toPromise()
      .then(response => {
        let res = response.json().data as Cronograma[];

        res.forEach(element => {
          element.dataEntrega = this.formataData(element.dataEntrega);
        });

        return res;
      })
      .catch(this.showError);
  }

  private showError(err: any): Promise<any> {
    return Promise.reject(err.message || err);
  }

  private formataData(date) {
    // return moment(date).add(1, 'd').format('MM/DD/YYYY');
    return moment(date).format('L');
  }

  private normalizeModel(cronograma: Cronograma) {

    function numberOnly(value) {
      if (typeof value === 'string') {
        return value.replace(/[^0-9]/g, '');
      }
    }

    function removerEspeciais(value) {
      if (typeof value === 'string') {
        return value.replace(/[^a-zA-Z1-9]/g, '');
      }
    }

    return {
    };
  }
}

