
export class Cronograma {

  public idCronograma: number;
  public nomeProduto: string;
  public nomeEscola: string;
  public dataEntrega;
  public quantidade: number;

  constructor() { }
}
