import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Produtor } from './produtorDetalhe.model';
import { ProdutorDetalheService } from './produtorDetalhe.service';

import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import emailMask from 'text-mask-addons/dist/emailMask';

import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-produtor-detalhe-detalhe',
  templateUrl: './produtorDetalhe-detalhe.component.html'
})
export class ProdutorDetalheComponent implements OnInit {

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  private editMode = false;
  produtor: Produtor;

  public idadeMask = [/[0-9]/, /\d/, /\d/];
  public maskCelular = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskTelefone = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskCep = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  public dapMask = [/[A-Z]/i, /[A-Z]/i, /[A-Z]/i, ' ', /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ',
    /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/];
  public emailMask = emailMask;
  public numeroMask = createNumberMask({
    prefix: '',
    suffix: '',
    thousandsSeparatorSymbol: '.',
    integerLimit: 9
  });

  constructor(
    private produtorDetalheService: ProdutorDetalheService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.produtor = new Produtor();
    // verifica se está logado
    if (localStorage.getItem('user')) {
      this.produtorDetalheService.find()
        .then((produtor: Produtor) => {
          this.produtor = produtor;
        })
        .catch((err) => {
          console.error(err);
          this._service.error('Erro', 'Ops, ocorreu um erro ao buscar o registro!', this.configGritter);
        });
    } else {
      this.router.navigateByUrl('login');
      this._service.error('Erro', 'É necessário estar logado para acessar essa página!', this.configGritter);
    }
  }

  onSubmit(): void {
    let promise;

    promise = this.produtorDetalheService.update(this.produtor);

    promise.then(produtor => {
      this._service.success('Sucesso', 'Operação Executada com Sucesso', this.configGritter);

      this.produtor = produtor;
    })
      .catch((err) => {
        console.log(err);
        this._service.error('Erro', 'Ops, algo deu errado ao salvar o registro!', this.configGritter);
      });

  }

  goBack(): void {
    this.router.navigateByUrl('/home');
  }
}
