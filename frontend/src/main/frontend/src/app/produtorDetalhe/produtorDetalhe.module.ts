import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

import { IconeModule } from '.././icone/icone.module';
import { MenuModule } from '.././menu/menu.module';
import { ProdutorDetalheComponent } from './produtorDetalhe-detalhe.component';
import { CronogramaComponent } from './cronograma.component';
import { ProdutorDetalheRoutingModule } from './produtorDetalhe-routing.module';
import { ProdutorDetalheService } from './produtorDetalhe.service';
import { CronogramaService } from './cronograma.service';

@NgModule({
  imports: [
    CommonModule,
    ProdutorDetalheRoutingModule,
    FormsModule,
    MenuModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    TabsModule,
    TextMaskModule,
    IconeModule,
  ],
  declarations: [
    ProdutorDetalheComponent,
    CronogramaComponent
  ],
  exports: [
    ProdutorDetalheComponent,
    CronogramaComponent
  ],
  providers: [
    ProdutorDetalheService,
    CronogramaService
  ]
})
export class ProdutorDetalheModule { }
