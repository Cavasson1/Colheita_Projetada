import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProdutorDetalheComponent } from './produtorDetalhe-detalhe.component';

const routes: Routes = [{
  path: 'produtor/save',
  component: ProdutorDetalheComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProdutorDetalheRoutingModule { }
