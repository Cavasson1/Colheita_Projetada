import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AlertModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';

import { Ng2InputMaskModule } from 'ng2-input-mask';
import { SimpleNotificationsModule, SimpleNotificationsComponent, NotificationsService } from 'angular2-notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './login/login.module';
import { DialogService } from './dialog.service';


@NgModule({
    imports: [
        AppRoutingModule,
        BrowserModule,
        LoginModule,
        HttpModule,
        BrowserAnimationsModule,
        NgbModule.forRoot(),
        SimpleNotificationsModule.forRoot(),
        AlertModule.forRoot(),
        BsDatepickerModule.forRoot()
    ],
    exports: [
        SimpleNotificationsComponent,
        BsDatepickerModule
    ],
    declarations: [AppComponent],
    providers: [
        DialogService,
        NotificationsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}
