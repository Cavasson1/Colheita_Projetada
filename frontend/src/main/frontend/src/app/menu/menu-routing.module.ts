import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CronogramaComponent } from '.././produtorDetalhe/cronograma.component';
import { ProdutorDetalheComponent } from '../produtorDetalhe/produtorDetalhe-detalhe.component';
import { HomeComponent } from '.././home/home.component';
import { EntregaListaComponent } from '.././entrega/entrega-lista.component';
import { EscolaListaComponent } from '.././escola/escola-lista.component';
import { ProdutoListaComponent } from '.././produto/produto-lista.component';
import { ProdutorListaComponent } from '.././produtor/produtor-lista.component';

const routes: Routes = [{
  path: 'home',
  component: HomeComponent
}, {
  path: 'produto',
  component: ProdutoListaComponent
}, {
  path: 'produtor',
  component: ProdutorListaComponent
}, {
  path: 'escola',
  component: EscolaListaComponent
}, {
  path: 'entrega',
  component: EntregaListaComponent
}, {
  path: 'cronograma',
  component: CronogramaComponent
}, {
  path: 'produtorDetalhe/save',
  component: ProdutorDetalheComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MenuRoutingModule { }
