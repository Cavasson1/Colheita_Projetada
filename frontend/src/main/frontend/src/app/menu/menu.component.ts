import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  private rota: string;
  tpUsuario: boolean;

  constructor(private route: Router) {}

  ngOnInit(): void {
    this.rota = this.route.url;
    this.tpUsuario = JSON.parse(localStorage.getItem('user')).permission === 'adm' ? true : false;
  }

}
