import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EscolaDetalheComponent } from './escola-detalhe.component';

const routes: Routes = [
    {
        path: 'escola/save',
        component: EscolaDetalheComponent
    },
    {
        path: 'escola/save/:id',
        component: EscolaDetalheComponent
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class EscolaRoutingModule { }
