import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Observable } from 'rxjs/Observable';

import { Escola } from './escola.model';
import { ServiceInterface } from './../interface/service.interface';

@Injectable()
export class EscolaService implements ServiceInterface<Escola> {
  private apiUrl = 'app/escola';
  private headers: Headers = new Headers({
    'Content-Type': 'application/json'
  });

  private escolas: Escola[];

  private api = 'http://localhost:8080';

  constructor(
    private http: Http
  ) { }

  findAll(): Promise<Escola[]> {
    return this.http
      .get(this.api + '/app/escola/listar')
      .toPromise()
      .then(response => {
        return response.json().data as Escola[];
      })
      .catch(this.showError);
  }

  find(id: number): Promise<Escola> {
    return this.http
      .get(this.api + '/app/escola/listar/' + id)
      .toPromise()
      .then(response => response.json().data as Escola[])
      .catch(this.showError);
  }

  create(escola: Escola): Promise<Escola> {
    return this.http
      .post(this.api + '/app/escola/salvar', JSON.stringify(this.normalizeModel(escola)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => response.json().data as Escola)
      .catch(this.showError);
  }

  update(escola: Escola): Promise<Escola> {
    return this.http
      .put(this.api + '/app/escola/salvar', JSON.stringify(this.normalizeModel(escola)), { headers: this.headers })
      .toPromise()
      .then((response: Response) => response.json().data as Escola)
      .catch(this.showError);
  }

  delete(escola: Escola): Promise<Escola> {
    return this.http
    .delete(this.api + '/app/escola/deletar/' + escola.idEscola, { headers: this.headers })
    .toPromise()
    .then((response: Response) => response.json().data as Escola)
    .catch(this.showError);
  }

  showError(err: any): Promise<any> {
    return Promise.reject(err.message || err);
  }

  private normalizeModel(escola: Escola) {

    function numberOnly(value) {
      if (typeof value === 'string') {
        return value.replace(/[^0-9]/g, '');
      }
    }

    return {
      idEscola: +escola.idEscola,
      nome: escola.nome,
      email: escola.email,
      telefone1: numberOnly(escola.telefone1),
      telefone2: numberOnly(escola.telefone2),
      rua: escola.rua,
      bairro: escola.bairro,
      cidade: escola.cidade,
      uf: escola.uf,
      cep: numberOnly(escola.cep),
      numero: numberOnly(escola.numero),
      complemento: escola.complemento
    };
  }
}
