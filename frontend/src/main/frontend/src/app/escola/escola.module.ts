import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

import { IconeModule } from '.././icone/icone.module';
import { MenuModule } from '.././menu/menu.module';
import { EscolaDetalheComponent } from './escola-detalhe.component';
import { EscolaListaComponent } from './escola-lista.component';
import { EscolaRoutingModule } from './escola-routing.module';
import { EscolaService } from './escola.service';

@NgModule({
  imports: [
    CommonModule,
    EscolaRoutingModule,
    FormsModule,
    MenuModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    TabsModule,
    TextMaskModule,
    IconeModule
  ],
  declarations: [
    EscolaListaComponent,
    EscolaDetalheComponent
  ],
  exports: [
    EscolaListaComponent
  ],
  providers: [
    EscolaService
  ]
})
export class EscolaModule { }
