import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Escola } from './escola.model';
import { EscolaService } from './escola.service';

import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import emailMask from 'text-mask-addons/dist/emailMask';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-escola-detalhe',
  templateUrl: './escola-detalhe.component.html'
})
export class EscolaDetalheComponent implements OnInit {

  private editMode = false;
  escola: Escola;

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  public maskTelefone = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/]
  public maskCep = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]
  public emailMask = emailMask;
  public numeroMask = createNumberMask({
    prefix: '',
    suffix: '',
    thousandsSeparatorSymbol: '.',
    integerLimit: 9
  });

  private id: number;

  constructor(
    private escolaService: EscolaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.escola = new Escola();

    // verifica se está logado
    if (localStorage.getItem('user')) {
      // verifica a permissão
      if (JSON.parse(localStorage.getItem('user')).permission === 'adm') {
        this.activatedRoute.params.forEach((params: Params) => {
          this.id = +params['id'];
          if (this.id) {
            this.editMode = true;
            this._buscar();
          }
        });
      } else {
        this.router.navigateByUrl('home');
        this._service.error('Erro', 'Você não tem permissão para acessar essa página!', this.configGritter);
      }
    } else {
      this.router.navigateByUrl('login');
      this._service.error('Erro', 'É necessário estar logado para acessar essa página!', this.configGritter);
    }
  }

  onSubmit(): void {
    let promise;

    if (this.editMode) {
      promise = this.escolaService.update(this.escola);
    } else {
      promise = this.escolaService.create(this.escola);
    }

    promise.then(escola => {
      const url = 'escola/save';
      this.router.navigateByUrl(`${url}/${escola.idEscola}`);
      this.escola = escola;
      this._service.success('Sucesso', 'Operação executada com sucesso!', this.configGritter);
    })
    .catch((err) => {
      console.log(err)
      this._service.error('Erro', 'Ops, algo deu errado ao salvar o registro!', this.configGritter);
    });
  }

  novo(): void {
    this.router.navigateByUrl('entrega/escola/new/' + this.escola.idEscola);
  }

  deletar(): void {
    let promise = this.escolaService.delete(this.escola);

    promise.then(escola => {
      this._service.success('Sucesso', 'Operação Executada com Sucesso', this.configGritter);
      this.router.navigateByUrl('/escola');
      this._buscar();
    });
  }

  _buscar(): void {
    this.escolaService.find(this.id)
    .then((escola: Escola) => {
      this.escola = escola;
    })
    .catch((err) => {
      console.error(err);
      this._service.error('Erro', 'Ops, ocorreu um erro ao buscar o registro!', this.configGritter);
    });
  }

  goBack(): void {
    this.router.navigateByUrl('/escola');
  }
}
