

export class Escola {

  public idEscola: number;
  public nome: string;
  public email: string;
  public telefone1: string;
  public telefone2: string;
  public rua: string;
  public bairro: string;
  public cidade: string;
  public uf: string;
  public cep: string;
  public numero: string;
  public complemento: string;

  constructor() { }
}
