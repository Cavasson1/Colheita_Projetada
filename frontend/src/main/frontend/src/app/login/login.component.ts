import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Login } from './login.model';
import { LoginService } from './login.service';

import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  private login: Login;
  private vldLogin = false;

  private configGritter = {
    timeOut: 3500,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
  };

  constructor(
    private router: Router,
    private loginService: LoginService,
    private _service: NotificationsService
  ) { }

  ngOnInit(): void {
    this.login = new Login();
    localStorage.clear();
  }

  logar(): void {
    this.loginService.login(this.login)
      .then((res) => {

        localStorage.setItem('user', JSON.stringify({'idProdutor': res.idUsuario, 'name': res.usuario, 'permission': res.permission}));

        this.router.navigateByUrl('/home');
        this._service.success('Sucesso', 'Login efetuado com sucesso!', this.configGritter);
      })
      .catch((err) => {
        this._service.error('Erro', err.message || 'Erro ao efetuar o login', this.configGritter);
        console.error(err.message || err);
        this.login = new Login();
      });
  }

}
