import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CadastroModule } from '.././cadastro/cadastro.module';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { LoginService } from './login.service';
import { HomeModule } from '.././home/home.module';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    HomeModule,
    FormsModule,
    CadastroModule
  ],
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    LoginService
]
})
export class LoginModule { }
