import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService {

    private api: string = 'http://localhost:8080';
    
    private url: string = '/app/login';
    private headers: Headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor(
        private http: Http
    ) { }

    login(login): Promise<any> {
        return this.http
            .post(this.api + this.url, JSON.stringify(login), { headers: this.headers })
            .toPromise()
            .then(response => response.json().data)
            .catch(this.showError);
    }

    showError(err: any): Promise<any> {
        return Promise.reject(err.message || err);
    }
}
