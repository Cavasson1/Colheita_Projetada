import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { EntregaListaComponent } from '.././entrega/entrega-lista.component';
import { EscolaListaComponent } from '.././escola/escola-lista.component';
import { ProdutoListaComponent } from '.././produto/produto-lista.component';
import { ProdutorListaComponent } from '.././produtor/produtor-lista.component';

import { ProdutorDetalheComponent } from '../produtorDetalhe/produtorDetalhe-detalhe.component';
import { CronogramaComponent } from '.././produtorDetalhe/cronograma.component';

const routes = [{
  path: 'home',
  component: HomeComponent
}, {
  path: 'produto',
  component: ProdutoListaComponent
}, {
  path: 'produtor',
  component: ProdutorListaComponent
}, {
  path: 'escola',
  component: EscolaListaComponent
}, {
  path: 'entrega',
  component: EntregaListaComponent
}, {
  path: 'produtorDetalhe/save',
  component: ProdutorDetalheComponent
}, {
  path: 'cronograma',
  component: CronogramaComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
