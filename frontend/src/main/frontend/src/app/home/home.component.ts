import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-component',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  tpUsuario: boolean;

  public constructor(
    private router: Router
  ) { }

  public ngOnInit(): void {
    if (!localStorage.getItem('user')) {
      this.router.navigateByUrl('login');
    } else {
      this.tpUsuario = JSON.parse(localStorage.getItem('user')).permission === 'adm' ? true : false;
    }
  }
}
