import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';
import { IconeModule } from '.././icone/icone.module';
import { HomeRoutingModule } from './home-routing.module';
import { EntregaModule } from '.././entrega/entrega.module';
import { EscolaModule } from '.././escola/escola.module';
import { ProdutoModule } from '.././produto/produto.module';
import { ProdutorModule } from '.././produtor/produtor.module';
import { ProdutorDetalheModule } from '.././produtorDetalhe/produtorDetalhe.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    ProdutoModule,
    ProdutorModule,
    EscolaModule,
    EntregaModule,
    IconeModule,
    ProdutorDetalheModule
  ],
  declarations: [
    HomeComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
